*Squares* is a simple application that implements a *square detector*, which is a program that looks for squares in an image and highlight them in the final result.

# Execution

- Start the BOSP daemon
	```
	[BOSPShell >] bbque-startd
	```

- Launch the application
	```
	[BOSPShell BOSP] \> squares --help
	Usage: squares [options]
	SquaresDetection Configuration Options:
  		--help                                produce help message
  		--image-path arg (=./test.jpg)        The image that will be used by the program
  		--canny-threshold arg (=50)           Upper threshold of Canny
  		--min-area arg (=1000)                Squares with the area less then 
  		                                      min-area will be ignored
  		--max-cosine arg (=0.300000012)       If the max cosine of the square is 
  		                                      greater than the value defined by this 
  		                                      option the square will be ignored
  		--contours-approx-precision arg (=0.0199999996)
  		                                      Precision of the contour approximation.
  		                                      The smaller the value the higher the 
  		                                      precision
  		--max-threshold arg (=11)             The searching will be performed with 
  		                                      different threshold levels, from 0 to 
  		                                      MAX.
  		--max-time arg (=500)                 Max execution time (milliseconds)
  		--cps-goal arg (=50)                  Use this flag to define the CPS Goal 
  		                                      that BOSP will use to run the program
  		--from-color-plane arg (=0)           The program will look for squares 
  		                                      starting from this color plane (min=0)
  		--to-color-plane arg (=2)             The program will look for squares 
    	                                      ending in this color plane (max=2)
	```

- Stop the BOSP daemon
	```
	[BOSPShell >] bbque-stopd
	```

The folder `test_images` contains some images that you can use to test the application.
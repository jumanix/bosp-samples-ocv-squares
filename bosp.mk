
ifdef CONFIG_SAMPLES_OPENCV_SQUARES

# Targets provided by this project
.PHONY: samples_opencv_squares clean_samples_opencv_squares

# Add this to the "samples" targets
samples_opencv: samples_opencv_squares
clean_samples_opencv: clean_samples_opencv_squares

MODULE_SAMPLES_OPENCV_SQUARES=samples/opencv/squares

samples_opencv_squares: external
	@echo
	@echo "==== Building Squares ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_SAMPLES_OPENCV_SQUARES)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_SAMPLES_OPENCV_SQUARES)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_SAMPLES_OPENCV_SQUARES)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_SAMPLES_OPENCV_SQUARES)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_samples_opencv_squares:
	@echo
	@echo "==== Clean-up Squares Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/squares ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/squares*; \
		rm -f $(BUILD_DIR)/usr/bin/squares*
	@rm -rf $(MODULE_SAMPLES_OPENCV_SQUARES)/build
	@echo

else # CONFIG_SAMPLES_OPENCV_SQUARES

samples_opencv_squares:
	$(warning Squares module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_SAMPLES_OPENCV_SQUARES


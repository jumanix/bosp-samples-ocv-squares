/**
 *       @file  Squares_exc.cc
 *      @brief  The Squares detector BarbequeRTRM application
 *
 * Description: This application looks for squares in an image
 *
 *     @author  Paolo Marazzi <paolo3.marazzi@mail.polimi.it>
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2020
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "Squares_exc.h"

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

#include <iostream>
#include <list>

using namespace std;
using namespace cv;
using namespace std;


// helper function:
// finds a cosine of angle between vectors
// from pt0->pt1 and from pt0->pt2

static double angle( Point pt1, Point pt2, Point pt0 )
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

// ----------------------------------------------------------------------------------------

Squares::Squares(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		string image_path,
        int max_threshold,
        int canny_threshold,
        int min_area,
        float max_cosine,
        float contours_approx_precision,
        int max_exec_time,
        float cps_goal,
        int from_color_plane,
        int to_color_plane) :
	BbqueEXC(name, recipe, rtlib, RTLIB_LANG_CPP),
	image_path(image_path),
    max_threshold(max_threshold),
    canny_threshold(canny_threshold),
    min_area(min_area),
    max_cosine(max_cosine),
    contours_approx_precision(contours_approx_precision),
    max_exec_time(max_exec_time),
    CPSGoal(cps_goal)
{
    logger->Warn("New Squares::Squares()");
    logger->Notice("EXC Unique IDentifier (UID): %u", GetUniqueID());
    
    // Checking --max-color-plane
    if (to_color_plane > 2 || from_color_plane < 0 || from_color_plane > to_color_plane){
        logger->Warn("Invalid color plane combination! Using from-color-plane=0, to-color-plane=2");
        max_color_plane = 2;
        current_color_plane = 0;
    } else {
        max_color_plane = to_color_plane;
        current_color_plane = from_color_plane;
    }
    
    current_threshold = 0;
    detection_time = 0;
    tot_execution_time = 0;
}

RTLIB_ExitCode_t Squares::onSetup() 
{
	logger->Warn("Squares::onSetup()");

    // Searching and reading the image from disk
    logger->Notice("Squares::onSetup(): Looking for: %s", image_path.c_str());
    string filename;
    try {
        filename = samples::findFile(image_path);
    }
    catch (...){
        logger->Error("Squares::onSetup(): Can't find the image %s", image_path.c_str());
        return RTLIB_ERROR;
    }
    
    image = imread(filename, IMREAD_COLOR);

    // Checking the image
    if( image.empty() )
    {
        logger->Error("Squares::onSetup(): Couldn't load %s", filename.c_str());
        return RTLIB_ERROR;
    }

    // down-scale and upscale the image to filter out the noise
    Mat pyr;
    pyrDown(image, pyr, Size(image.cols/2, image.rows/2));
    pyrUp(pyr, timg, image.size());    

    return RTLIB_OK;
}

RTLIB_ExitCode_t Squares::onConfigure(int8_t awm_id) 
{
    logger->Warn("Squares::onConfigure()");

	int32_t proc_nr;    // nr. of CPU cores 
	int32_t proc_quota; // CPU quota (e.g. quota = 235% -> 3 CPU cores)
	int32_t acc, gpu;
	GetAssignedResources(PROC_ELEMENT, proc_quota);
	GetAssignedResources(PROC_NR, proc_nr);
	GetAssignedResources(GPU, gpu);
	GetAssignedResources(ACCELERATOR, acc);

    SetCPSGoal(CPSGoal - 1, CPSGoal + 1);

    logger->Notice("Squares::onConfigure(): Proc Quota=%d", proc_quota);
    logger->Notice("Squares::onConfigure(): Proc nr=%d",proc_nr);
    logger->Notice("Squares::onConfigure(): GPU=%d", gpu);
    logger->Notice("Squares::onConfigure(): Accellerator=%d", acc);
    logger->Notice("Squares::onConfigure(): CPSGoal=%.2f", CPSGoal);

	return RTLIB_OK;
}

RTLIB_ExitCode_t Squares::onRun() {

	logger->Warn("Squares::onRun(): Cycle=%d", Cycles());
    logger->Notice("Squares::onRun(): Threshold: %d, Color plane: %d", current_threshold, current_color_plane);
    double t=0;

    t = (double)getTickCount();

    Mat gray0(image.size(), CV_8U), gray;

    vector<vector<Point> > contours;
    
    // find squares in every color plane of the image
    int ch[] = {current_color_plane, 0};
    mixChannels(&timg, 1, &gray0, 1, ch, 1);

    if( current_threshold == 0 )
    {
        // apply Canny. Take the upper threshold from slider
        // and set the lower to 0 (which forces edges merging)
        Canny(gray0, gray, 0, canny_threshold, 5);
        // dilate canny output to remove potential
        // holes between edge segments
        dilate(gray, gray, Mat(), Point(-1,-1));
    }
    else
    {
        // tgray(x,y) = gray(x,y) < (l+1)*255/N ? 255 : 0
        gray = gray0 >= (current_threshold+1)*255/max_threshold;
    }

    // find contours and store them all as a list
    vector<Point> approx;
    findContours(gray, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
    // test each contour
    for( size_t i = 0; i < contours.size(); i++ )
    {
        // approximate contour with accuracy proportional
        // to the contour perimeter
        approxPolyDP(contours[i], approx, arcLength(contours[i], true)*contours_approx_precision, true);
        // square contours should have 4 vertices after approximation
        // relatively large area (to filter out noisy contours)
        // and be convex.
        // Note: absolute value of an area is used because
        // area may be positive or negative - in accordance with the
        // contour orientation
        if( approx.size() == 4 &&
            fabs(contourArea(approx)) > min_area &&
            isContourConvex(approx) )
        {
            double maxCosine = 0;
            for( int j = 2; j < 5; j++ )
            {
                // find the maximum cosine of the angle between joint edges
                double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                maxCosine = MAX(maxCosine, cosine);
            }
            // if cosines of all angles are small
            // (all angles are ~90 degree) then write quandrange
            // vertices to resultant sequence
            if( maxCosine < max_cosine )
                squares.push_back(approx);
        }
    }

    t = (double)getTickCount() - t;
    detection_time = t*1000/getTickFrequency();

    return RTLIB_OK;
}

RTLIB_ExitCode_t Squares::onMonitor() 
{
    logger->Warn("Squares::onMonitor()");

    tot_execution_time += detection_time;

	logger->Notice("Squares::onMonitor(): CPS=%.2f (Goal=%.2f)", GetCPS(), CPSGoal);
    logger->Notice("Squares::onMonitor(): Latency last run=%.2f", detection_time);
    logger->Notice("Squares::onMonitor(): Tot Execution Time=%.2f", tot_execution_time);
    logger->Notice("Squares::onMonitor(): Max Execution Time=%d", max_exec_time);

    // Checking if we have reached the max execution time
    if (tot_execution_time >= max_exec_time){
        logger->Warn("Squares::onMonitor(): Timeout!");
        return RTLIB_EXC_WORKLOAD_NONE;
    }

    // Checking if we have reached the max level
    if (current_threshold >= max_threshold){
        // Checking if we are in the last color plane. 
        // If yes, it means that we have done! 
        if (current_color_plane == max_color_plane){
            return RTLIB_EXC_WORKLOAD_NONE;
        }
        else
        {
            // Change color plane and reset the current threshold
            current_color_plane++;
            current_threshold = 0;
        }
    }
    else{
        // Update the threshold
        current_threshold++;
    }

	return RTLIB_OK;
}

RTLIB_ExitCode_t Squares::onSuspend() 
{
    logger->Warn("Squares::onSuspend()");
	return RTLIB_OK;
}

RTLIB_ExitCode_t Squares::onRelease() 
{
    logger->Warn("Squares::onRelease()");

    if (image.empty()){ 
        logger->Warn("Squares::onRelease(): Nothing to show");
        return RTLIB_OK;
    }


    polylines(image, squares, true, Scalar(0, 255, 0), 3, LINE_AA); // Highlight the squares
    imshow("Squares Detection Demo", image);
    waitKey();
		
	return RTLIB_OK;
}
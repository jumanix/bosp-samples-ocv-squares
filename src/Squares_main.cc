/**
 *       @file  squares_main.cc
 *      @brief  The Squares detector BarbequeRTRM application
 *
 * Description: This application looks for squares in an image
 *
 *     @author  Paolo Marazzi <paolo3.marazzi@mail.polimi.it>
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2020
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <libgen.h>
#include <iostream>
#include <memory>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>

#include "Squares_exc.h"

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "Squares"

using namespace std;
namespace po = boost::program_options;

std::unique_ptr<bu::Logger> logger;
std::string conf_file = BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/Squares.conf" ;


po::options_description opts_desc("SquaresDetection Configuration Options");
po::variables_map opts_vm;

int canny_threshold;
int min_area;
int max_threshold;
int max_time;
int from_color_plane;
int to_color_plane;
float max_cosine;
float contours_approx_precision;
float csp_goal;
string image_path;


void ParseCommandLine(int argc, char *argv[]){
	
	try {
	po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]) 
{
	opts_desc.add_options()
    	("help", "produce help message")
    	("image-path", po::value<string>(&image_path)-> default_value("./test.jpg"),
    		"The image that will be used by the program")
    	("canny-threshold", po::value<int>(&canny_threshold)-> default_value(50)
    		, "Upper threshold of Canny")
    	("min-area", po::value<int>(&min_area)-> default_value(1000), 
    		"Squares with the area less then min-area will be ignored")
    	("max-cosine", po::value<float>(&max_cosine)-> default_value(0.3), 
    		"If the max cosine of the square is greater than the value defined by this option the square will be ignored")
    	("contours-approx-precision", po::value<float>(&contours_approx_precision)-> default_value(0.02), 
    		"Precision of the contour approximation. The smaller the value the higher the precision")
    	("max-threshold", po::value<int>(&max_threshold)-> default_value(11), 
    		"The searching will be performed with different threshold levels, from 0 to MAX.")
    	("max-time", po::value<int>(&max_time)-> default_value(500), 
    		"Max execution time (milliseconds)")
    	("cps-goal", po::value<float>(&csp_goal)-> default_value(50), 
    		"Use this flag to define the CPS Goal that BOSP will use to run the program")
    	("from-color-plane", po::value<int>(&from_color_plane)-> default_value(0), 
    		"The program will look for squares starting from this color plane (min=0)")
    	("to-color-plane", po::value<int>(&to_color_plane)-> default_value(2), 
    		"The program will look for squares ending in this color plane (max=2)")
	;

	// Setup a logger
	bu::Logger::SetConfigurationFile(conf_file);
	logger = bu::Logger::GetLogger("squares");


	ParseCommandLine(argc, argv);


	RTLIB_Services_t *rtlib;
	auto ret = RTLIB_Init(basename(argv[0]), &rtlib);
	if (ret != RTLIB_OK) {
		logger->Error("ERROR: Unable to init RTLib (Did you start the BarbequeRTRM daemon?)");
		return RTLIB_ERROR;
	}
	assert(rtlib);

	std::string recipe("squares");
	logger->Notice("INFO: Registering EXC with recipe %s", recipe.c_str());
	auto pexc = std::make_shared<Squares>("Squares", recipe, rtlib, 
		image_path, 
		max_threshold,
		canny_threshold,
		min_area,
		max_cosine,
		contours_approx_precision,
		max_time,
		csp_goal,
		from_color_plane,
		to_color_plane);
	if (!pexc->isRegistered()) {
		logger->Error("ERROR: Register failed (missing the recipe file?)");
		return RTLIB_ERROR;
	}

	logger->Notice("INFO: Starting EXC control thread");
	pexc->Start();

	logger->Notice("INFO: Waiting for the EXC termination");
	pexc->WaitCompletion();

	logger->Notice("INFO: Terminated. ");
	return EXIT_SUCCESS;
}

/**
 * =====================================================================================
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef SQUARES_VERSION_H_
#define SQUARES_VERSION_H_

extern "C" {

/**
 * A string representing the GIT version of the compiled binary
 */
extern const char *g_git_version;

}

#endif // SQUARES_VERSION_H_


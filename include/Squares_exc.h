/**
 *       @file  squares_exc.h
 *      @brief  The Squares detector BarbequeRTRM application
 *
 * Description: This application looks for squares in an image
 *
 *     @author  Paolo Marazzi <paolo3.marazzi@mail.polimi.it>
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2020
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef SQUARES_EXC_H_
#define SQUARES_EXC_H_

#include <bbque/bbque_exc.h>

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

#include <vector>
#include <list>

using bbque::rtlib::BbqueEXC;
using namespace cv;
using namespace std;

class Squares : public BbqueEXC {

public:

	Squares(std::string const & name,
		    std::string const & recipe,
		    RTLIB_Services_t *rtlib,
		    string image_path,
		    int max_threshold,
		    int canny_threshold,
		    int min_area,
			float max_cosine,
			float contours_approx_precision,
			int max_exec_time,
			float cps_goal,
			int from_color_plane,
			int to_color_plane);

private:

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onSuspend();
	RTLIB_ExitCode_t onRelease();

	// Data structure/objects supporting the execution
	vector<vector<Point>> squares; // The result

	Mat image; // The image loaded from disk
	Mat timg; // The image loaded from disk (scaled up and down)	
	string image_path; // The image path passed as cmd line arg 

	int max_threshold;
	int canny_threshold;
	int min_area;
	float max_cosine;
	float contours_approx_precision;
	int max_exec_time;
	int current_threshold;
	int current_color_plane;
	int max_color_plane;

	double detection_time;
	double tot_execution_time;
	float CPSGoal;
};

#endif // SQUARES_EXC_H_

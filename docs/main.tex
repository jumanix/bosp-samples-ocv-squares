\documentclass[a4paper,12pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Packages
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=blue,
}

\usepackage[english]{babel}

\usepackage{graphicx}
\graphicspath{ {./images/} }

\usepackage{longtable}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Squares Documentation}
\author{Paolo Marazzi \\ Matricola: 940182 \\ Email: \href{mailto:paolo3.marazzi@mail.polimi.it}{paolo3.marazzi@mail.polimi.it}}
\date{}

\begin{document}

\maketitle

\tableofcontents
\newpage


The purpose of this document is to explain the goal of the project, its implementation and how to use the application.

The first section presents the goals of the project. The second section explains the design decisions that have been taken and how they have been translated into code. Finally, the last section is a simple User Guide that presents the different options of the program and how they influence the quality and the performances of the execution.

If you are interested only in the usage of the application you can directly jump to the User Guide section.
\newpage

\section{Introduction}

The goal of this project is to implement the porting of an OpenCV/C++ sample program into the Adaptive Execution Model of the BOSP Framework. The OpenCV sample named ``squares'' has been chosen to perform the porting.
You can find the original code of the sample \href{https://docs.opencv.org/master/db/d00/samples\_2cpp\_2squares\_8cpp-example.html}{here}.
\\
The code linked above implements a \textit{square detector} which is an application that detects all (or most of all) the squares in the image and highlight them in the result presented at the end of the execution. 
The following picture is an example of the final result.
\\\\
\includegraphics[width=\textwidth]{example_result}

\subsection{Execution Flow}
The high-level execution flow of the sample is the following. The target image is loaded form disk and for each one of the three color planes different threshold levels are applied. The squares are detected for each threshold level in each color plane.

Finally at the end of the loop all the detected squares are highlighted on the result image.

The picture below shows the execution flow of the sample.

\includegraphics[width=\textwidth, height=\textheight]{Sample_flowchart}

Canny is an edge detection algorithm. OpecnCV provides its implementation of the algorithm. You can find more information \href{https://docs.opencv.org/master/da/d22/tutorial_py_canny.html}{here}. \\
The different threshold levels are applied with the following logic:
\begin{verbatim}
tgray(x,y) = gray(x,y) < (l+1)*255/N ? 255 : 0
\end{verbatim}
Where:
\begin{itemize}
	\item \textit{tgray} is an 8-bit single channel mask whose elements are set to 255 (if the particular element or pair of elements satisfy the condition) or 0.
	\item \textit{gray} is the image on which the threshold is applied.
	\item \textit{l} is the threshold level that is applied
	\item \textit{N} is the maximum threshold level
\end{itemize}

The identification of the contours is performed with \href{https://docs.opencv.org/3.4/d3/dc0/group__imgproc__shape.html#ga17ed9f5d79ae97bd4c7cf18403e1689a}{cv::findContours} and the approximation is performed with \href{https://docs.opencv.org/3.4/d3/dc0/group__imgproc__shape.html#ga0012a5fdaea70b8a9970165d98722b4c}{cv::approxPolyDP}.
\\\\
More interesting is the definition of ``square'' provided by the algorithm. A generic polygon is a square if all the following conditions are true:
\begin{itemize}
	\item the polygon has four sides
	\item the area of the polygon is at least 1000
	\item the maximum cosine between all the cosines of the corners of the polygon is not greater than 0.3  
\end{itemize}

If all the conditions above are true, the polygon is considered a square and it will be highlighted in the final result.

\section{Design \& Implementation}
This section will explain how the code of the sample has been mapped to the  functions implemented by the class \textit{Squares}  extending the class \textit{BbqueEXC}.

\subsection{Adaptive Execution Model}
The first function that is called by the execution thread of the framework is the function \textbf{OnSetup}. In this function the image is loaded form disk and scaled up and down in order to filter out the noise. Notice that, if the image is not found, the function returns \textit{RTLIB ERROR} and the execution is terminated.
\\\\
The function \textbf{OnConfigure} doesn't contain any code from the sample. It just defines the \textit{CPSGoal}.
\\\\
The function \textbf{OnRun} performs a single step of computation, which means looking for squares in a color plane of the image applying a threshold.
For example, the function could look for squares in the color plane 2 applying the threshold 5. The next call, it will look in the same color plane applying the threshold 6 and so on.
Another operation performed by the function \textbf{OnRun} is tracking the time needed to perform the search. Such value will be used in the function \textbf{OnMonitor} to compute the \textit{Total Execution Time}.     
\\\\
The function \textbf{OnMonitor} checks:
\begin{itemize}
	\item if all the thresholds have been applied on all the color planes. 
	\item if the \textit{Total Execution Time} has reached the \textit{Maximum Execution Time}. The \textit{Maximum Execution Time} will be explained in the next section.
\end{itemize}

If one of the two conditions is true the execution is terminated, otherwise the function updates the variables defining which threshold should be applied and in which control plane at the next call of \textbf{OnRun}. \\
The variables are updated with the following logic: if the \textit{current threshold} is not equal to the \textit{maximum threshold} then the \textit{current threshold} is updated, otherwise if the \textit{current color plane} is not the last the \textit{current threshold} is reset and the \textit{current color plane} is updated. If we are in the last color plane and the \textit{current threshold} is equal to the \textit{maximum threshold} it means that the execution is terminated.\\
In conclusion, the function \textbf{OnMonitor} implements the first two \textit{for loops} that you find in the flow graph presented in the previous section.\\
The next picture shows the flow graph of the control logic implemented in the function \textbf{OnMonitor}.
\\\\
Finally, the function \textbf{OnRelease} is used to highlight the squares on the image and to show the result.

\begin{center}
\includegraphics[scale=0.6]{Update_logic}
\end{center}

The picture below shows how the original code of the sample is mapped in the event functions of the Adaptive Execution Model.

\includegraphics[width=\textwidth, height=\textheight]{Sample_mapped_flowchart}

It is worth spending few words on the decision of not using threads. It could be possible to implement the application parallelizing the search, for example using a producer-consumer patters with \textit{N} consumers, where \textit{N} is the number of threads that the application can use. But in my opinion the overhead introduced by the threads and the code complexity required to implement that type of pattern is not convenient for two main reasons: first, the original sample fits very well, as it is, in the Adaptive Execution Model. Second, the application doesn't perform high-load computation so it cannot really take advantage of the parallel execution.   

\subsection{Additional Features}
The original code of the sample has some hard coded values. In the integrated version of the application these values are transformed into command line options that the user can exploit in order to control the quality of the final result. All the options are presented in the User Guide section.
\\\\
The hard coded values are the following:
\begin{itemize}
	\item the image(s) that will be used to perform the search
	\item the max threshold value (default = 11)
	\item the color planes where the search will be performed (default = all $\rightarrow$ 0,1,2)
	\item the threshold used in the Canny algorithm (default = 50)
	\item the accuracy in the contour approximation (default = 0.02)
	\item the minimum area to consider the polygon acceptable (default = 1000)
	\item the max cosine to consider the angles of 90 degrees (default = 0.3) 
\end{itemize}

The user has also the possibility to chose the \textit{CPSGoal} and the \textit{Maximum Execution Time}. The \textit{CPSGoal} is defined when the function \textbf{OnConfigure} is called and, the class \textit{Squares} takes track of the total time spent in the function \textbf{OnRun}. These two parameter, controllable by the relative command line flags, offer to the user a little bit of control on the performance of the application. As explained in the previous section, the function \textbf{OnMonitor} checks if the \textit{Maximum Execution Time} has been reached. If so, the execution is terminated even if the search is not complete!
\\\\
Notice that all the values defined in the bullet list above, but the \textit{max threshold} and the \textit{color planes}, don't influence the performance of the execution but they just define the quality of the final result. The \textit{max threshold} and the \textit{color planes} instead influence the number of iterations (\textbf{OnRun} calls) that are performed. Obviously these parameters will be critical in the definition of the right trade-off between quality and performances. 

\section{User Guide}

This section is a quick User Guide of the application. You don't need to read the other sections to understand the content of this one.
\\\\
\textit{Squares} is a simple application that implements a \textit{square detector}, which is a program that looks for squares in an image and highlight them in the final result.
\begingroup
\fontsize{10pt}{12pt}\selectfont
\begin{verbatim}
[BOSPShell BOSP] \> squares --help
Usage: squares [options]
SquaresDetection Configuration Options:
  --help                                produce help message
  --image-path arg (=./test.jpg)        The image that will be used by the 
                                        program
  --canny-threshold arg (=50)           Upper threshold of Canny
  --min-area arg (=1000)                Squares with the area less then 
                                        min-area will be ignored
  --max-cosine arg (=0.300000012)       If the max cosine of the square is 
                                        greater than the value defined by this 
                                        option the square will be ignored
  --contours-approx-precision arg (=0.0199999996)
                                        Precision of the contour approximation.
                                        The smaller the value the higher the 
                                        precision
  --max-threshold arg (=11)             The searching will be performed with 
                                        different threshold levels, from 0 to 
                                        MAX.
  --max-time arg (=500)                 Max execution time (milliseconds)
  --cps-goal arg (=50)                  Use this flag to define the CPS Goal 
                                        that BOSP will use to run the program
  --from-color-plane arg (=0)           The program will look for squares 
                                        starting from this color plane (min=0)
  --to-color-plane arg (=2)             The program will look for squares 
                                        ending in this color plane (max=2)
\end{verbatim}
\endgroup

The following table explains the options that the user can use to control the quality of the output and the performance of the application. For each option is given a description and it is specified if it affects the the quality (Q) and/or the performance (P). Notice that if an option influences only the quality it doesn't mean that the ``maximum value'' of that option will give you the best result. For example, if you define a very high \textit{approximation precision} you will probably miss a lot of squares because of the very low approximation error that you are requiring. The same concept can be applied to all the other options that can control the quality of the result. 
\\\\
The general rule is that, there is not a good/bad ``direction'' (increment/decrement) of the value of an option, but what you have to do is to find the value, or the combination of values, that will give you an acceptable result.
\\\\
\begin{longtable}{ |p{5.5cm}||p{7cm}|p{0.5cm}|p{0.5cm}|  }
 \hline
 \multicolumn{4}{|c|}{\textbf{Command Line Options}} \\
 \hline
 Option& Description& P& Q \\
 \hline
 --image-path& The absolute or relative path of the target image& -& -\\
 --canny-threshold& \href{http://en.wikipedia.org/wiki/Canny_edge_detector}{Canny algorithm}: second threshold for the hysteresis procedure& -& X\\
 --min-area& A relative large value can be used to filter out noisy contours. Remember that if the target image contains small squares and you are using a value that is to much large some squares will be missed& -& X\\
 --max-cosine& You can use this value to define the quality of the squares that you are going to detect. The greater this value the higher the number of parallelogram that you are going to accept as squares in the final result. The smaller this value the higher the number of squares that will be discarded& -& X\\
 --contours-approx-precision& The approximation of the contours is performed with an accuracy proportional to the contour perimeter. The accuracy is the maximum distance between the original curve and its approximation. The value defined by this option is multiplied by the perimeter to define the accuracy used in the \href{http://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm}{Ramer-Douglas-Peucker algorithm}& -& X\\
 --max-threshold& The maximum threshold that will be applied (starting from 0). The greater this number the greater the execution time. This value will modify the quality of the output. Be aware of the fact that all the threshold are applied before changing the color plane. Which means that, if this number is relative large it is possible to reach the max execution time and so the search will be terminated without searching in each color plane.& X& X\\
 --max-time& When this values is reached the computation will be terminated. Obviously if the search is not completed the quality of the output is not the best possible& X& X\\
 --cps-goal& The CPS Goal used by BOSP& X& -\\
 --from-color-plane& You can use this option and the following one to specify in which color planes performing the search. Skipping one/two color plane(s) will decrease the quality of the result but it will also reduce the execution time& X& X\\ 
 --to-color-plane& & &\\
 \hline
\end{longtable}

\end{document}

